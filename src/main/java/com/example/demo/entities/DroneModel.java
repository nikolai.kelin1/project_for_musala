package com.example.demo.entities;

import com.example.demo.enums.DroneEnum;
import com.example.demo.enums.State;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "drone")
public class DroneModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Max(100)
    private Integer serialNumber;
    @Enumerated(EnumType.STRING)
    private DroneEnum model;
    @Max(500)
    private Integer weightLimit;
    @Column(precision = 5, scale = 2)
    private BigDecimal batteryCapacity;
    @Enumerated(EnumType.STRING)
    private State state;

    @OneToMany(mappedBy = "droneId", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<MedicationModel> medications;
}
