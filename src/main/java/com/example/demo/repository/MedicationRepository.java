package com.example.demo.repository;

import com.example.demo.entities.MedicationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationRepository extends JpaRepository<MedicationModel, Long> {
    MedicationModel findByCode(String code);
    List<MedicationModel> findAllByDroneId(Long id);
}
