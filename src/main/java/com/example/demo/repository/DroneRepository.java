package com.example.demo.repository;

import com.example.demo.entities.DroneModel;
import com.example.demo.enums.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DroneRepository extends JpaRepository<DroneModel, Long> {
    List<DroneModel> findAllByStateOrState(State idle, State loading);
}
