package com.example.demo.mapper;

import com.example.demo.dto.Medication;
import com.example.demo.entities.MedicationModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MedicationMapper {

    public List<Medication> mapToMedications(List<MedicationModel> medicationModels) {
        List<Medication> medicationList = new ArrayList<>();
        for (var medicationModel : medicationModels) {
            medicationList.add(mapToMedication(medicationModel));
        }
        return medicationList;
    }

    public List<MedicationModel> mapToModels(List<Medication> medications) {
        List<MedicationModel> medicationModelsList = new ArrayList<>();
        for (var medication : medications) {
            medicationModelsList.add(mapToModel(medication));
        }
        return medicationModelsList;
    }

    public MedicationModel mapToModel(Medication medication) {
        var medicationModel = new MedicationModel();
        medicationModel.setId(medication.getId());
        medicationModel.setName(medication.getName());
        medicationModel.setCode(medication.getCode());
        medicationModel.setWeight(medication.getWeight());
        medicationModel.setImage(medication.getImage());
//        medicationModel.setDroneModel(medication.getDroneModel());
        return medicationModel;
    }

    public Medication mapToMedication(MedicationModel medicationModel) {
        Medication medication = new Medication();
        medication.setId(medicationModel.getId());
        medication.setName(medicationModel.getName());
        medication.setCode(medicationModel.getCode());
        medication.setWeight(medicationModel.getWeight());
        medication.setImage(medicationModel.getImage());
//        medication.setDroneModel(medicationModel.getDroneModel());
        return medication;
    }
}
