package com.example.demo.mapper;

import com.example.demo.dto.Drone;
import com.example.demo.entities.DroneModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DroneMapper {

    public List<Drone> mapToDrones(List<DroneModel> droneModels) {
        List<Drone> droneList = new ArrayList<>();
        for (var droneModel : droneModels) {
            droneList.add(mapToDrone(droneModel));
        }
        return droneList;
    }

    public Drone mapToDrone(DroneModel droneModel) {
        var drone = new Drone();
        drone.setId(droneModel.getId());
        drone.setState(droneModel.getState());
        drone.setBatteryCapacity(droneModel.getBatteryCapacity());
        drone.setWeightLimit(droneModel.getWeightLimit());
        drone.setSerialNumber(droneModel.getSerialNumber());
        drone.setModel(droneModel.getModel());
        drone.setMedications(droneModel.getMedications());
        return drone;
    }

    public DroneModel mapToModel(Drone drone) {
        var droneModel = new DroneModel();
        droneModel.setId(drone.getId());
        droneModel.setState(drone.getState());
        droneModel.setBatteryCapacity(drone.getBatteryCapacity());
        droneModel.setWeightLimit(drone.getWeightLimit());
        droneModel.setSerialNumber(drone.getSerialNumber());
        droneModel.setModel(drone.getModel());
        droneModel.setMedications(drone.getMedications());
        return droneModel;
    }

    public List<DroneModel> mapToModels(List<Drone> drones) {
        List<DroneModel> droneList = new ArrayList<>();
        for (var drone : drones) {
            droneList.add(mapToModel(drone));
        }
        return droneList;
    }

}
