package com.example.demo.log;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Setter
@Getter
public class EventLog {

    private final List<Event> events = new ArrayList<>();

    public void addEvent(Event event) {
        events.add(event);
    }

    @Getter
    @Setter
    public static class Event {
        private final String message;
        private final long timestamp;

        public Event(String message) {
            this.message = message;
            this.timestamp = System.currentTimeMillis();
        }
    }
}
