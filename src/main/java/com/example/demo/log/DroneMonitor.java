package com.example.demo.log;

import com.example.demo.entities.DroneModel;
import com.example.demo.repository.DroneRepository;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class DroneMonitor {
    private static final String FILE_NAME = "event_log.txt";

    private final EventLog eventLog;
    private final DroneRepository droneRepository;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public DroneMonitor(EventLog eventLog, DroneRepository droneRepository) {
        this.eventLog = eventLog;
        this.droneRepository = droneRepository;
    }

    public void startMonitoring() {
        scheduler.scheduleAtFixedRate(() -> {
            List<Map<Long, BigDecimal>> maps = checkBatteryLevels();
            createEventLog(maps);
        }, 0, 1, TimeUnit.MINUTES);
    }

    private List<Map<Long, BigDecimal>> checkBatteryLevels() {
        List<DroneModel> all = droneRepository.findAll();
        List<Map<Long, BigDecimal>> mapDroneIdBatteryCapacity = new ArrayList<>();
        for (var droneModel : all) {
            Map<Long, BigDecimal> map = new HashMap<>();
            map.put(droneModel.getId(), droneModel.getBatteryCapacity());
            mapDroneIdBatteryCapacity.add(map);
        }
        return mapDroneIdBatteryCapacity;
    }

    private void createEventLog(List<Map<Long, BigDecimal>> maps) {

        for (var map : maps) {
            for (var entry : map.entrySet()) {
                var droneId = entry.getKey();
                var batteryLevel = entry.getValue();
                String message = String.format("Drone %s: Battery level is %s%%", droneId, batteryLevel.toString());
                eventLog.addEvent(new EventLog.Event(message));
            }
        }
        saveEventLog(eventLog);
    }

    private void saveEventLog(EventLog eventLog) {
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(FILE_NAME, true))) {
            writer.println("===================================================");
            writer.println(new Date());
            for (var event : eventLog.getEvents()) {
                writer.println(event.getMessage());
            }
            writer.println(new Date());
            writer.println("===================================================");
        } catch (IOException e) {
            System.out.println("Error saving event log to file: " + e.getMessage());
        }
    }
}
