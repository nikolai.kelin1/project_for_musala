package com.example.demo.service;

import com.example.demo.dto.Drone;
import com.example.demo.entities.DroneModel;
import com.example.demo.entities.MedicationModel;
import com.example.demo.enums.State;
import com.example.demo.log.DroneMonitor;
import com.example.demo.mapper.DroneMapper;
import com.example.demo.mapper.MedicationMapper;
import com.example.demo.repository.DroneRepository;
import com.example.demo.repository.MedicationRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;


@Service
public class DroneService {

    private final DroneRepository droneRepository;
    private final DroneMapper droneMapper;
    private final MedicationRepository medicationRepository;
    private final DroneMonitor droneMonitor;

    public DroneService(DroneRepository droneRepository, DroneMapper droneMapper,
                        MedicationRepository medicationRepository, DroneMonitor droneMonitor) {
        this.droneRepository = droneRepository;
        this.droneMapper = droneMapper;
        this.medicationRepository = medicationRepository;
        this.droneMonitor = droneMonitor;
    }

    public List<Drone> getAllDrones() {
        var allDrones = droneRepository.findAll();
        return droneMapper.mapToDrones(allDrones);
    }

    public Drone getDroneById(Long id) {
        var drone = droneRepository.findById(id).orElse(null);
        return droneMapper.mapToDrone(drone);
    }

    public String getBatteryDroneById(Long id) {
        var drone = droneRepository.findById(id).orElse(null);
        return drone.getBatteryCapacity() + "%";
    }

    public List<Drone> getAllAvailableForLoadingDrones() {
        var allDrones = droneRepository.findAllByStateOrState(State.IDLE, State.LOADING);
        return droneMapper.mapToDrones(allDrones);
    }

    public Drone saveDrone(Drone drone) {
        var saved = droneRepository.save(droneMapper.mapToModel(drone));
        return droneMapper.mapToDrone(saved);
    }

    @Transactional
    public Drone setDrugToDrone(Long id, String code) {
        droneMonitor.startMonitoring();
        var droneModel = droneRepository.findById(id).orElse(null);
        var medicationModel = medicationRepository.findByCode(code);

        if (droneModel == null || medicationModel == null) {
            throw new RuntimeException("Drone or medication not found");
        }

        if (!isMedicationAvailableToLoad(medicationModel)) {
            throw new RuntimeException("Medication " + medicationModel.getName() + " already loaded");
        }

        if (droneModel.getState() != State.IDLE && droneModel.getState() == State.LOADING) {
            throw new RuntimeException("Drone " + droneModel.getSerialNumber() + " is busy");
        }

        if (cannotBeLoaded(droneModel)) {
            throw new RuntimeException("Drone's charge is too low(<25%)");
        }

        if (isNotEnoughWeightCapacity(droneModel, medicationModel)) {
            throw new RuntimeException("Sorry, drone does not have enough weight capacity");
        }

        medicationModel.setDroneId(droneModel.getId());
        droneModel.setState(State.LOADING);
        droneRepository.saveAndFlush(droneModel);
        return droneMapper.mapToDrone(droneModel);
    }

    private boolean cannotBeLoaded(DroneModel droneModel) {
        return droneModel.getBatteryCapacity().compareTo(new BigDecimal(25)) < 0;
    }

    private boolean isNotEnoughWeightCapacity(DroneModel droneModel, MedicationModel medicationModel) {
        var alreadyUploadedWeight = 0;
        var medications = droneModel.getMedications();
        for (var medication :
                medications) {
            alreadyUploadedWeight += medication.getWeight();
        }
        return droneModel.getWeightLimit() - alreadyUploadedWeight - medicationModel.getWeight() < 0;
    }

    private boolean isMedicationAvailableToLoad(MedicationModel medicationModel) {
        return medicationModel.getDroneId() == null;
    }
}
