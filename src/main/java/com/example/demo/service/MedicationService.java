package com.example.demo.service;

import com.example.demo.dto.Medication;
import com.example.demo.entities.MedicationModel;
import com.example.demo.mapper.MedicationMapper;
import com.example.demo.repository.DroneRepository;
import com.example.demo.repository.MedicationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;
    private final MedicationMapper medicationMapper;
    private final DroneRepository droneRepository;

    public MedicationService(MedicationRepository medicationRepository, MedicationMapper medicationMapper,
                             DroneRepository droneRepository) {
        this.medicationRepository = medicationRepository;
        this.medicationMapper = medicationMapper;
        this.droneRepository = droneRepository;
    }

    public List<Medication> getAllMedication() {
        var all = medicationRepository.findAll();
        return medicationMapper.mapToMedications(all);
    }

    public Medication getMedicationById(Long id) {
        var medication = medicationRepository.findById(id).orElse(null);
        return medicationMapper.mapToMedication(medication);
    }

    public Medication updateMedication(Medication medication) {
        Long id = medication.getId();
        var retrievedMedication = new MedicationModel();
        if (id != null) {
            retrievedMedication = medicationRepository.getById(id);
            if (medication.getImage() != null) {
                retrievedMedication.setImage(medication.getImage());
            }
            if (medication.getWeight() != null) {
                retrievedMedication.setWeight(medication.getWeight());
            }
            if (medication.getCode() != null) {
                retrievedMedication.setCode(medication.getCode());
            }
            if (medication.getName() != null) {
                retrievedMedication.setName(medication.getName());
            }
            medicationRepository.saveAndFlush(retrievedMedication);
        }
        return medicationMapper.mapToMedication(retrievedMedication);
    }

    public Medication saveMedication(Medication medication) {
        var medicationModel = medicationMapper.mapToModel(medication);
        var saved = medicationRepository.save(medicationModel);
        return medicationMapper.mapToMedication(saved);
    }

    public List<Medication> getDroneLoadsById(Long id) {
        var droneModel = droneRepository.findById(id).orElse(null);
        if (droneModel == null) {
            throw new RuntimeException("Not found drone with id = " + id);
        }
        var allByDroneId = medicationRepository.findAllByDroneId(droneModel.getId());

        return medicationMapper.mapToMedications(allByDroneId);
    }


    public void deleteMedication(Long id) {
        medicationRepository.deleteById(id);
    }
}
