package com.example.demo.dto;

import com.example.demo.entities.DroneModel;
import lombok.Data;


@Data
public class Medication {

    private Long id;
    private String name;
    private Integer weight;
    private String code;
    private String image;
    private DroneModel droneModel;
}
