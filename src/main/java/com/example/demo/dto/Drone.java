package com.example.demo.dto;

import com.example.demo.entities.MedicationModel;
import com.example.demo.enums.DroneEnum;
import com.example.demo.enums.State;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Set;

@Data
public class Drone {

    private Long id;
    private Integer serialNumber;
    private Integer weightLimit;
    private BigDecimal batteryCapacity;
    private DroneEnum model;
    private State state;
    private Set<MedicationModel> medications;
}
