package com.example.demo.controllers;

import com.example.demo.dto.Drone;
import com.example.demo.service.DroneService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class DroneController {

    private DroneService droneService;

    public DroneController(DroneService droneService) {
        this.droneService = droneService;
    }

    @GetMapping(value = "/drones", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody List<Drone> getDrones() {
        return droneService.getAllDrones();
    }

    @GetMapping(value = "/drone/{id}", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody Drone getDroneById(@PathVariable(value = "id") Long id) {
        return droneService.getDroneById(id);
    }

    @PostMapping(value = "drone")
    public Drone createDrone(@Valid @RequestBody Drone drone) {
         return droneService.saveDrone(drone);
    }

    @PostMapping(value = "drone/{id}" )
    public Drone downloadDrone(@Valid @PathVariable(value = "id") Long id, @RequestParam String code) {
        return droneService.setDrugToDrone(id, code);
    }

    @GetMapping(value = "/drones/available", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody List<Drone> getDronesForLoading() {
        return droneService.getAllAvailableForLoadingDrones();
    }

    @GetMapping(value = "/drone/{id}/battery", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody String getBatteryDroneById(@PathVariable(value = "id") Long id) {
        return droneService.getBatteryDroneById(id);
    }
}
