package com.example.demo.controllers;

import com.example.demo.dto.Drone;
import com.example.demo.dto.Medication;
import com.example.demo.entities.MedicationModel;
import com.example.demo.service.MedicationService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class MedicationController {

    private MedicationService medicationService;

    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping(value = "/medication", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody List<Medication> getMedications() {
        return medicationService.getAllMedication();
    }

    @GetMapping(value = "/medication/{id}", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody Medication getMedicationById(@PathVariable(value = "id") Long id) {
        return medicationService.getMedicationById(id);
    }

    @PutMapping(value = "/medication" )
    public Medication updateMedication(@RequestBody Medication medication) {
        return medicationService.updateMedication(medication);
    }

    @GetMapping(value = "/medication/loads/{id}", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody List<Medication> getDroneLoadsById(@PathVariable(value = "id") Long id) {
        return medicationService.getDroneLoadsById(id);
    }
}
