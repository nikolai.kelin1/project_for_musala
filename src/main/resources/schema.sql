DROP TABLE IF EXISTS drone;
-- Create drone table
CREATE TABLE drone (
                       id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                       serial_number INTEGER,
                       model VARCHAR(255),
                       weight_limit INTEGER,
                       battery_capacity DECIMAL(5,2),
                       state VARCHAR(255)
);

DROP TABLE IF EXISTS medication;
-- Create medication table
CREATE TABLE medication (
                            id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                            name VARCHAR(255),
                            weight INTEGER,
                            code VARCHAR(255),
                            image VARCHAR(255),
                            drone_id BIGINT,
                            CONSTRAINT FK_medication_drone FOREIGN KEY (drone_id) REFERENCES drone (id)
);