### Java Spring project for Musala Soft

For run this project just run commands:
1) mvn clean install
2) mvn spring-boot:run


Also, there is in resources a Postman Collection file ProjectForMusalaSoft.postman_collection.json. 
It is convenient to use it to test API manually.